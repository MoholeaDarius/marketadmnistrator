package com.example.demo;

import com.example.demo.controller.Controller;
import com.example.demo.repository.*;
import com.example.demo.view.LoginView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage window;

    @Override
    public void start(Stage stage) throws Exception {
        UserRepository userRepository = new UserRepository();
        ProductRepository productRepository = new ProductRepository();
        ExpensesRepository expensesRepository = new ExpensesRepository();
        CashingRepository cashingRepository = new CashingRepository();
        ShopRepository shopRepository = new ShopRepository(productRepository, expensesRepository, cashingRepository, userRepository);
        CompanyRepository companyRepository = new CompanyRepository(shopRepository);
        Controller controller = new Controller(companyRepository,shopRepository, userRepository, productRepository, expensesRepository, cashingRepository);
        this.window = stage;
        window.setTitle("MegaImage");
        window.setScene(new Scene(new LoginView(controller),280,300));
        window.show();
    }


    public static void main(String[] args) {
        launch();
    }
}