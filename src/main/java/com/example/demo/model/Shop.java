package com.example.demo.model;

import java.util.List;
import java.util.Objects;

public class Shop {

    private int id;
    private String name;
    private String address;
    private List<Product> products;
    private List<Expenses> expenses;
    private List<Cashing> cashingList;
    private List<User> users;

    public Shop(int id, String name, String address, List<Product> products, List<Expenses> expenses, List<Cashing> cashingList, List<User> users) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.products = products;
        this.expenses = expenses;
        this.cashingList = cashingList;
        this.users = users;
    }

    public Shop(String name, String address, List<Product> products, List<Expenses> expenses, List<Cashing> cashingList, List<User> users) {
        this.name = name;
        this.address = address;
        this.products = products;
        this.expenses = expenses;
        this.cashingList = cashingList;
        this.users = users;
    }

    public Shop(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Expenses> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expenses> expenses) {
        this.expenses = expenses;
    }

    public List<Cashing> getCashingList() {
        return cashingList;
    }

    public void setCashingList(List<Cashing> cashingList) {
        this.cashingList = cashingList;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shop shop = (Shop) o;
        return id == shop.id && Objects.equals(name, shop.name) && Objects.equals(address, shop.address) && Objects.equals(products, shop.products) && Objects.equals(expenses, shop.expenses) && Objects.equals(cashingList, shop.cashingList) && Objects.equals(users, shop.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, products, expenses, cashingList, users);
    }

    @Override
    public String toString() {
        return name;
    }

}