package com.example.demo.model;

public enum UserStatus {

    active, inactive, disabled, restricted

}
