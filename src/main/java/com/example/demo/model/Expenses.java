package com.example.demo.model;

public class Expenses {

    private int id;
    private TypeOfExpense typeOfExpenses;
    private String nameOfExpenses;
    private int quantity;
    private double valuePerOne;
    private double totalValue;

    public Expenses(int id, TypeOfExpense typeOfExpenses, String nameOfExpenses, int quantity, double valuePerOne, double totalValue) {
        this.id = id;
        this.typeOfExpenses = typeOfExpenses;
        this.nameOfExpenses = nameOfExpenses;
        this.quantity = quantity;
        this.valuePerOne = valuePerOne;
        this.totalValue = totalValue;
    }

    public Expenses(TypeOfExpense typeOfExpenses, String nameOfExpenses, int quantity, double valuePerOne, double totalValue) {
        this.typeOfExpenses = typeOfExpenses;
        this.nameOfExpenses = nameOfExpenses;
        this.quantity = quantity;
        this.valuePerOne = valuePerOne;
        this.totalValue = totalValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TypeOfExpense getTypeOfExpenses() {
        return typeOfExpenses;
    }

    public void setTypeOfExpenses(TypeOfExpense typeOfExpenses) {
        this.typeOfExpenses = typeOfExpenses;
    }

    public String getNameOfExpenses() {
        return nameOfExpenses;
    }

    public void setNameOfExpenses(String nameOfExpenses) {
        this.nameOfExpenses = nameOfExpenses;
    }

    public double getValuePerOne() {
        return valuePerOne;
    }

    public void setValuePerOne(double valuePerOne) {
        this.valuePerOne = valuePerOne;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    @Override
    public String
    toString() {
        return "Type: " + getTypeOfExpenses() + "Quantity" + getQuantity() + "Value" + getValuePerOne() + "";
    }

}
