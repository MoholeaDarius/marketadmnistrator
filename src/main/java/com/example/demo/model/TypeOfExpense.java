package com.example.demo.model;

public enum TypeOfExpense {

    Product, AuxiliaryExpense

}
