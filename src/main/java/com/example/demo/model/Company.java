package com.example.demo.model;

import java.util.List;
import java.util.Objects;

public class Company {

    private int id;
    private String name;
    private List<Shop> shops;

    public Company(int id, String name, List<Shop> shops) {
        this.id = id;
        this.name = name;
        this.shops = shops;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Shop> getShops() {
        return shops;
    }

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Objects.equals(name, company.name) && Objects.equals(shops, company.shops);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, shops);
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", magazines=" + shops +
                '}';
    }

}
