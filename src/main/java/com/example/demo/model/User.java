package com.example.demo.model;

import java.util.List;

public class User {

    private int id;
    private String name;
    private String surname;
    private int cnp;
    private int age;
    private List<Integer> shopIds;
    private String username;
    private String password;
    private String role;
    private String status;
    private String profilePicture;

    public User(int id, String name, String surname, int cnp, int age, List<Integer> shopIds, String username, String password, String role, String status, String profilePicture) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.cnp = cnp;
        this.age = age;
        this.shopIds = shopIds;
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
        this.profilePicture = profilePicture;
    }

    public User(String name, String surname, int cnp, int age, List<Integer> shopIds, String username, String password, String role, String status, String profilePicture) {
        this.name = name;
        this.surname = surname;
        this.cnp = cnp;
        this.age = age;
        this.shopIds = shopIds;
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
        this.profilePicture = profilePicture;
    }

    public User(String username, String password, String role, String status, String profilePicture) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
        this.profilePicture = profilePicture;
    }

    public User(String username, String password, String role, String status) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getCnp() {
        return cnp;
    }

    public void setCnp(int cnp) {
        this.cnp = cnp;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Integer> getShopIds() {
        return shopIds;
    }

    public void setShopIds(List<Integer> shopIds) {
        this.shopIds = shopIds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", cnp=" + cnp +
                ", age=" + age +
                ", shopIds=" + shopIds +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", status='" + status + '\'' +
                ", profilePicture='" + profilePicture + '\'' +
                '}';
    }

}
