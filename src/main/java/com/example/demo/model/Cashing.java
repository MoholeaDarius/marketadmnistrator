package com.example.demo.model;

public class Cashing {

    private int id;
    private String nameOfProduct;
    private int quantity;
    private double valuePerOne;
    private double totalValue;

    public Cashing(int id, String nameOfProduct, int quantity, double valuePerOne, double totalValue) {
        this.id = id;
        this.nameOfProduct = nameOfProduct;
        this.quantity = quantity;
        this.valuePerOne = valuePerOne;
        this.totalValue = totalValue;
    }

    public Cashing(String nameOfProduct, int quantity, double valuePerOne, double totalValue) {
        this.nameOfProduct = nameOfProduct;
        this.quantity = quantity;
        this.valuePerOne = valuePerOne;
        this.totalValue = totalValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public double getValuePerOne() {
        return valuePerOne;
    }

    public void setValuePerOne(double valuePerOne) {
        this.valuePerOne = valuePerOne;
    }

}
