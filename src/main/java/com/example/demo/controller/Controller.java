package com.example.demo.controller;

import com.example.demo.model.*;
import com.example.demo.repository.*;

import java.util.ArrayList;
import java.util.List;

public class Controller {

    private CompanyRepository companyRepository;
    private ShopRepository shopRepository;
    private UserRepository userRepository;
    private ProductRepository productRepository;
    private ExpensesRepository expensesRepository;
    private CashingRepository cashingRepository;

    public Controller(CompanyRepository companyRepository, ShopRepository shopRepository, UserRepository userRepository, ProductRepository productRepository, ExpensesRepository expensesRepository, CashingRepository cashingRepository) {
        this.companyRepository = companyRepository;
        this.shopRepository = shopRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.expensesRepository = expensesRepository;
        this.cashingRepository = cashingRepository;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Company getCompany() {
        return companyRepository.findCompany();
    }

    public Shop findShopById(int id) {
        return shopRepository.findById(id);
    }

    public List<Shop> getAllShops() {
        return shopRepository.findAll();
    }

    public List<User> getAllAdmins() {
        List<User> users = new ArrayList<>();
        for (User user : getAllUsers()) {
            if (user.getRole().equals("admin")) {
                users.add(user);
            }
        }
        return users;
    }

    public List<User> getAllManagers() {
        List<User> users = new ArrayList<>();
        for (User user : getAllUsers()) {
            if (user.getRole().equals("manager")) {
                users.add(user);
            }
        }
        return users;
    }

    public List<User> getAllEmployees() {
        List<User> users = new ArrayList<>();
        for (User user : getAllUsers()) {
            if (user.getRole().equals("employee")) {
                users.add(user);
            }
        }
        return users;
    }

    public void addNewUser(User user) {
        userRepository.insert(user);
    }

    public void deleteUserById(int id) {
        userRepository.deleteById(id);
    }

    public void updateUser(User user) {
        userRepository.update(user);
    }

    public void addCashing(Cashing cashing, int shopId) {
        cashingRepository.insert(cashing, shopId);
    }

    public List<Cashing> getAllCashing() {
        return cashingRepository.findAll();
    }

    public Shop getShopByName(String shopName) {
        return shopRepository.getShopByName(shopName);
    }

    public List<Expenses> getAllExpenses() {
        return expensesRepository.findAll();
    }

    public void addNewShop(Shop shop) {
        shopRepository.insert(shop);
    }

    public void deleteShopById(int id) {
        shopRepository.deleteById(id);
    }

    public void updateShop(Shop shop) {
        shopRepository.update(shop);
    }

    public void deleteExpenseById(int id) {
        expensesRepository.deleteById(id);
    }

    public void addExpenses(Expenses expenses, int shopId) {
        expensesRepository.insert(expenses, shopId);
    }


}