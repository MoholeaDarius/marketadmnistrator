package com.example.demo.view;

import com.example.demo.controller.Controller;
import com.example.demo.model.Product;
import com.example.demo.model.Shop;
import com.example.demo.model.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ProductsComponent {

    private TableView<Product> tableView;
    private Controller controller;
    private User user;


    public Node build(Controller controller, User user) {
        tableView = new TableView<>();
        this.controller = controller;
        this.user = user;
        tableView.setMinHeight(450);
        tableView.setMinWidth(520);
        tableView.setMaxWidth(520);

        TableColumn<Product, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setMinWidth(50);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Product, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(120);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Product, Double> priceColumn = new TableColumn<>("Price");
        priceColumn.setMinWidth(100);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

        TableColumn<Product, Integer> quantityColumn = new TableColumn<>("Quantity");
        quantityColumn.setMinWidth(50);
        quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        tableView.getItems().addAll(getProducts());
        tableView.getColumns().addAll(idColumn, nameColumn, priceColumn, quantityColumn);

        Label shops = new Label("  Shops");
        shops.setFont(new Font(13));
        shops.setMinWidth(54);
        shops.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        shops.setTextFill(Color.ROYALBLUE);

        Button allShopsButton = new Button("All cashings");
        allShopsButton.setMinWidth(150);
        allShopsButton.setOnAction(e -> {
            tableView.getItems().clear();
            tableView.getItems().addAll(getProducts());
        });

        List<Button> buttonList = buildButtonList(getShops());
        for (Button button : buttonList) {
            button.setMinWidth(150);
            button.setOnAction(e -> {
                tableView.getItems().clear();
                tableView.getItems().addAll(controller.getShopByName(button.getText()).getProducts());
            });
        }

        InputStream stream = null;
        try {
            stream = new FileInputStream("C:\\Users\\Moholea\\Pictures\\reclamaJavaFx2.png");
        } catch (FileNotFoundException e) {
        }
        Image image = new Image(stream);
        ImageView imageView = new ImageView();
        imageView.setImage(image);
        imageView.setX(10);
        imageView.setY(10);
        imageView.setPreserveRatio(true);

        VBox inputLayout = new VBox();
        inputLayout.getChildren().addAll(shops, allShopsButton);
        inputLayout.getChildren().addAll(buttonList);
        inputLayout.setSpacing(10);
        inputLayout.setAlignment(Pos.TOP_CENTER);

        HBox hBox = new HBox(tableView, inputLayout, imageView);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }

    private List<Button> buildButtonList(List<Shop> shops) {
        List<Button> buttonsList = new ArrayList<>();
        for (Shop shop : shops) {
            buttonsList.add(new Button(shop.getName()));
        }
        return buttonsList;
    }

    private List<Shop> getShops() {
        List<Shop> shops = new ArrayList<>();
        for (Shop shop : controller.getAllShops()) {
            if (user.getShopIds().contains(shop.getId())){
                shops.add(shop);
            }
        }
        return shops;
    }

    private List<Product> getProducts() {
        List<Product> products = new ArrayList<>();
        for (Shop shops : controller.getAllShops()) {
            if (user.getShopIds().contains(shops.getId())){
                products.addAll(shops.getProducts());
            }
        }
        return products;
    }

}
