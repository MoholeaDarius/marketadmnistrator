package com.example.demo.view;

import com.example.demo.Main;
import com.example.demo.controller.Controller;
import com.example.demo.model.User;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

import java.util.List;

public class LoginView extends Parent {

    private final Controller controller;
    private static GridPane gridPane;

    public LoginView(Controller controller) {
        this.controller = controller;
        getChildren().add(buildParent());
    }

    private Parent buildParent() {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(5);
        gridPane.setPadding(new Insets(10, 10, 10, 10));


        Label loginText = new Label("Log In");
        loginText.setFont(new Font(18));
        gridPane.add(loginText, 2, 0);


        Label username = new Label("Username: ");
        gridPane.add(username, 2, 1);

        TextField usernameInput = new TextField("Pablosa");
        usernameInput.setPromptText("username");
        gridPane.add(usernameInput, 3, 1);

        Label password = new Label("Password: ");
        gridPane.add(password, 2, 2);

        PasswordField passwordInput = new PasswordField();
        passwordInput.setText("123456");
        passwordInput.setPromptText("password");
        gridPane.add(passwordInput, 3, 2);

        Button logInButton = new Button("LogIn");
        logInButton.setOnAction(e -> {
            if (verifyIfUserExist(usernameInput, passwordInput)) {
                logInFromRole(getTheUserFromUsernameAndPassword(usernameInput, passwordInput));
            }
        });
        gridPane.add(logInButton, 3, 3);
        return gridPane;
    }

    private void logInFromRole(User user) {
        if (user.getRole().equals("admin")) {
            Main.window.setScene(new Scene(new AdminMainView(controller, user), 882, 514));
        } else if (user.getRole().equals("manager")) {
            Main.window.setScene(new Scene(new ManagerMainView(controller, user), 882, 514));
        } else if (user.getRole().equals("employee")) {
            Main.window.setScene(new Scene(new EmployeeMainView(controller, user), 882, 514));
        }
    }

    private boolean verifyIfUserExist(TextField usernameInput, TextField passwordInput) {
        if (verifyUserInputsAreEmpty(usernameInput, passwordInput)) {
            if (checkIfAccountExist(usernameInput, passwordInput, controller.getAllUsers())) {
                if (checkAccounts(usernameInput, passwordInput, controller.getAllUsers()).getStatus().equals("active")){
                    return true;
                }else{
                    AlertBox.displaySimple("LogIn error", "This account is: " + checkAccounts(usernameInput, passwordInput, controller.getAllUsers()).getStatus());
                }
            } else {
                AlertBox.displaySimple("LogIn error", "Wrong username or password!");
            }
        }
        return false;
    }

    private User getTheUserFromUsernameAndPassword(TextField usernameInput, TextField passwordInput) {
        User user = null;
        if (verifyUserInputsAreEmpty(usernameInput, passwordInput)) {
            if (checkIfAccountExist(usernameInput, passwordInput, controller.getAllUsers())) {
                user = checkAccounts(usernameInput, passwordInput, controller.getAllUsers());
            }
        }
        return user;
    }

    private boolean verifyUserInputsAreEmpty(TextField usernameInput, TextField passwordInput) {
        if (usernameInput.getText().isEmpty() && passwordInput.getText().isEmpty()) {
            AlertBox.displaySimple("LogIn error", "You forgot to enter an username and a password! ");
            return false;
        }
        if (usernameInput.getText().isEmpty()) {
            AlertBox.displaySimple("LogIn error", "You forgot to enter an username!");
            return false;
        }
        if (passwordInput.getText().isEmpty()) {
            AlertBox.displaySimple("LogIn error", "You forgot to enter a password!");
            return false;
        }
        return true;
    }

    public boolean checkIfAccountExist(TextField usernameInput, TextField passwordInput, List<User> users) {
        for (User user : users) {
            if (usernameInput.getText().equals(user.getUsername())) {
                if (passwordInput.getText().equals(user.getPassword())) {
                    return true;
                }
            }
        }
        return false;
    }

    public User checkAccounts(TextField usernameInput, TextField passwordInput, List<User> users) {
        for (User user : users) {
            if (usernameInput.getText().equals(user.getUsername())) {
                if (passwordInput.getText().equals(user.getPassword())) {
                    return user;
                }
            }
        }
        return null;
    }

}
