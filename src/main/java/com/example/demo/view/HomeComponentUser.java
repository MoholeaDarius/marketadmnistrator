package com.example.demo.view;

import com.example.demo.controller.Controller;
import com.example.demo.model.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class HomeComponentUser {

    private Controller controller;

    public Node build(Controller controller, User user) {
        this.controller = controller;
        InputStream stream = null;
        try {
            stream = new FileInputStream(user.getProfilePicture());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Image image = new Image(stream);
        ImageView imageView = new ImageView();
        imageView.setImage(image);
        imageView.setX(10);
        imageView.setY(10);
        imageView.setFitWidth(200);
        imageView.setFitHeight(200);
        imageView.setPreserveRatio(true);

        Label personalData = new Label("Personal data");
        personalData.setMinWidth(58);
        personalData.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        personalData.setTextFill(Color.ROYALBLUE);

        Label idLabel = new Label("Id:");
        idLabel.setMinWidth(58);
        TextField idLabelCompleted = new TextField(user.getId() + "");
        idLabelCompleted.setMinWidth(58);
        idLabelCompleted.setEditable(false);
        HBox idRow = new HBox();
        idRow.setSpacing(10);
        idRow.getChildren().addAll(idLabel, idLabelCompleted);

        Label nameLabel = new Label("Name:");
        nameLabel.setMinWidth(58);
        TextField nameLabelCompleted = new TextField(user.getName());
        nameLabelCompleted.setMinWidth(58);
        nameLabelCompleted.setEditable(false);
        HBox nameRow = new HBox();
        nameRow.setSpacing(10);
        nameRow.getChildren().addAll(nameLabel, nameLabelCompleted);

        Label surnameLabel = new Label("Surname:");
        surnameLabel.setMinWidth(58);
        TextField surnameLabelComplected = new TextField(user.getSurname());
        surnameLabelComplected.setMinWidth(58);
        surnameLabelComplected.setEditable(false);
        HBox surnameRow = new HBox();
        surnameRow.setSpacing(10);
        surnameRow.getChildren().addAll(surnameLabel, surnameLabelComplected);

        Label ageLabel = new Label("Age:");
        ageLabel.setMinWidth(58);
        TextField ageLabelComplected = new TextField(user.getAge() + "");
        ageLabelComplected.setMinWidth(58);
        ageLabelComplected.setEditable(false);
        HBox ageRow = new HBox();
        ageRow.setSpacing(10);
        ageRow.getChildren().addAll(ageLabel, ageLabelComplected);

        Label userData = new Label("User data:");
        userData.setMinWidth(58);
        userData.setFont(new Font(14));
        userData.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        userData.setTextFill(Color.ROYALBLUE);

        Label usernameLabel = new Label("Username:");
        usernameLabel.setMinWidth(58);
        TextField usernameLabelComplected = new TextField(user.getUsername());
        usernameLabelComplected.setMinWidth(58);
        HBox usernameRow = new HBox();
        usernameLabelComplected.setEditable(false);
        usernameRow.setSpacing(10);
        usernameRow.getChildren().addAll(usernameLabel, usernameLabelComplected);

        Label passwordLabel = new Label("Password:");
        passwordLabel.setMinWidth(58);
        TextField passwordLabelComplected = new TextField(user.getPassword());
        passwordLabelComplected.setMinWidth(58);
        passwordLabelComplected.setEditable(false);
        HBox passwordRow = new HBox();
        passwordRow.setSpacing(10);
        passwordRow.getChildren().addAll(passwordLabel, passwordLabelComplected);

        Label userRoleLabel = new Label("User Role:");
        userRoleLabel.setMinWidth(58);
        TextField userRoleLabelComplected = new TextField(user.getRole());
        userRoleLabelComplected.setMinWidth(58);
        userRoleLabelComplected.setEditable(false);
        HBox userRoleRow = new HBox();
        userRoleRow.setSpacing(10);
        userRoleRow.getChildren().addAll(userRoleLabel, userRoleLabelComplected);

        Label userStatusLabel = new Label("User status:");
        userStatusLabel.setMinWidth(58);
        TextField userStatusLabelComplected = new TextField(user.getStatus());
        userStatusLabelComplected.setMinWidth(58);
        usernameLabelComplected.setEditable(false);
        HBox userStatusRow = new HBox();
        userStatusRow.setSpacing(10);
        userStatusRow.getChildren().addAll(userStatusLabel, userStatusLabelComplected);

        Label imageLabel = new Label("User profil Image");
        imageLabel.setMinWidth(58);
        imageLabel.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        imageLabel.setTextFill(Color.ROYALBLUE);

        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.getChildren().addAll(personalData, idRow, nameRow, surnameRow, ageRow, userData, usernameRow, passwordRow, userRoleRow, userStatusRow);

        VBox vBox2 = new VBox();
        vBox2.setSpacing(10);
        vBox2.getChildren().addAll(imageLabel, imageView);

        HBox hBox = new HBox(vBox, vBox2);
        hBox.setSpacing(100);
        hBox.setMinHeight(470);
        hBox.setMinWidth(800);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }


}
