package com.example.demo.view;

import com.example.demo.model.Cashing;
import com.example.demo.model.Shop;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import com.example.demo.controller.Controller;
import javafx.scene.text.FontWeight;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CashingComponentForAdmin {

    private TableView<Cashing> tableView;
    private Controller controller;


    public Node build(Controller controller) {
        tableView = new TableView<>();
        this.controller = controller;
        tableView.setMinHeight(450);
        tableView.setMinWidth(520);

        TableColumn<Cashing, Integer> idProductColumn = new TableColumn<>("Id");
        idProductColumn.setMinWidth(50);
        idProductColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Cashing, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(100);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("nameOfProduct"));

        TableColumn<Cashing, Integer> quantityColumn = new TableColumn<>("Quantity");
        quantityColumn.setMinWidth(50);
        quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        TableColumn<Cashing, Integer> valuePerOneColumn = new TableColumn<>("Value per one");
        valuePerOneColumn.setMinWidth(70);
        valuePerOneColumn.setCellValueFactory(new PropertyValueFactory<>("valuePerOne"));

        TableColumn<Cashing, Integer> totalValueColumn = new TableColumn<>("Total Value");
        totalValueColumn.setMinWidth(70);
        totalValueColumn.setCellValueFactory(new PropertyValueFactory<>("totalValue"));

        tableView.getItems().addAll(controller.getAllCashing());
        tableView.getColumns().addAll(idProductColumn, nameColumn, quantityColumn, valuePerOneColumn, totalValueColumn);

        Label shops = new Label("  Shops");
        shops.setFont(new Font(13));
        shops.setMinWidth(54);
        shops.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        shops.setTextFill(Color.ROYALBLUE);

        Button allShopsButton = new Button("All chasing");
        allShopsButton.setMinWidth(150);
        allShopsButton.setOnAction(e -> {
            tableView.getItems().clear();
            tableView.getItems().addAll(controller.getAllCashing());
        });

        List<Button> buttonList = buildButtonList(controller.getCompany().getShops());
        for (Button button : buttonList) {
            button.setMinWidth(150);
            button.setOnAction(e -> {
                tableView.getItems().clear();
                tableView.getItems().addAll(controller.getShopByName(button.getText()).getCashingList());
            });
        }


        InputStream stream = null;
        try {
            stream = new FileInputStream("C:\\Users\\Moholea\\Pictures\\reclamaJafaFx.png");
        } catch (FileNotFoundException e) {
        }
        Image image = new Image(stream);
        ImageView imageView = new ImageView();
        imageView.setImage(image);
        imageView.setX(10);
        imageView.setY(10);
        imageView.setPreserveRatio(true);

        VBox inputLayout = new VBox();
        inputLayout.getChildren().addAll(shops, allShopsButton);
        inputLayout.getChildren().addAll(buttonList);
        inputLayout.setSpacing(10);
        inputLayout.setAlignment(Pos.TOP_CENTER);

        HBox hBox = new HBox(tableView, inputLayout, imageView);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }

    private List<Button> buildButtonList(List<Shop> shops) {
        List<Button> buttonsList = new ArrayList<>();
        for (Shop shop : shops){
            buttonsList.add(new Button(shop.getName()));
        }
        return buttonsList;
    }

}
