package com.example.demo.view;

import com.example.demo.controller.Controller;
import com.example.demo.model.Expenses;
import com.example.demo.model.Shop;
import com.example.demo.model.TypeOfExpense;
import com.example.demo.model.User;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.ArrayList;
import java.util.List;

public class ExpensesComponentForManager {

    private TableView<Expenses> tableView;
    private Controller controller;
    private User user;
    private Integer shopId;


    public Node build(Controller controller, User user) {
        tableView = new TableView<>();
        this.controller = controller;
        this.user = user;
        tableView.setMinHeight(450);
        tableView.setMinWidth(520);
        tableView.setMaxWidth(520);

        TableColumn<Expenses, Integer> idCostColumn = new TableColumn<>("Id");
        idCostColumn.setMinWidth(50);
        idCostColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Expenses, String> typeCostColumn = new TableColumn<>("Name of expense");
        typeCostColumn.setMinWidth(75);
        typeCostColumn.setCellValueFactory(new PropertyValueFactory<>("nameOfExpenses"));

        TableColumn<Expenses, String> nameCostColumn = new TableColumn<>("Type of cost");
        nameCostColumn.setMinWidth(50);
        nameCostColumn.setCellValueFactory(new PropertyValueFactory<>("typeOfExpenses"));

        TableColumn<Expenses, Integer> quantityCostColumn = new TableColumn<>("Quantity");
        quantityCostColumn.setMinWidth(50);
        quantityCostColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        TableColumn<Expenses, Double> valuePerOneCostColumn = new TableColumn<>("Value per one");
        valuePerOneCostColumn.setMinWidth(50);
        valuePerOneCostColumn.setCellValueFactory(new PropertyValueFactory<>("valuePerOne"));

        TableColumn<Expenses, Double> totalValueCostColumn = new TableColumn<>("Total value");
        totalValueCostColumn.setMinWidth(60);
        totalValueCostColumn.setCellValueFactory(new PropertyValueFactory<>("totalValue"));

        tableView.getColumns().addAll(idCostColumn, typeCostColumn, nameCostColumn, quantityCostColumn, valuePerOneCostColumn, totalValueCostColumn);
        tableView.getItems().addAll(getExpenses());

        Label emptySpace = new Label("");

        Label type = new Label("Type:");
        type.setMinWidth(56);
        ChoiceBox<String> typeOfExpense = new ChoiceBox<>();
        typeOfExpense.getItems().addAll(TypeOfExpense.Product.name(), TypeOfExpense.AuxiliaryExpense.name());
        typeOfExpense.setValue(TypeOfExpense.Product.name());
        typeOfExpense.setMinWidth(150);
        HBox typeRow = new HBox();
        typeRow.setSpacing(10);
        typeRow.getChildren().addAll(type, typeOfExpense);

        Label name = new Label("Name:");
        name.setMinWidth(56);
        TextField nameInput = new TextField();
        nameInput.setPromptText("Name");
        nameInput.setMinWidth(56);
        HBox nameRow = new HBox();
        nameRow.setSpacing(10);
        nameRow.getChildren().addAll(name, nameInput);

        Label quantity = new Label("Quantity:");
        quantity.setMinWidth(56);
        TextField quantityInput = new TextField();
        quantityInput.setPromptText("quantity");
        quantityInput.setMinWidth(56);
        HBox quantityRow = new HBox();
        quantityRow.setSpacing(10);
        quantityRow.getChildren().addAll(quantity, quantityInput);

        Label valuePerOne = new Label("Price:");
        valuePerOne.setMinWidth(56);
        TextField valuePerOneInput = new TextField();
        valuePerOneInput.setPromptText("value per one");
        valuePerOneInput.setMinWidth(56);
        HBox valuePerOneRow = new HBox();
        valuePerOneRow.setSpacing(10);
        valuePerOneRow.getChildren().addAll(valuePerOne, valuePerOneInput);

        tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                typeOfExpense.setValue(newSelection.getTypeOfExpenses().name());
                nameInput.setText(newSelection.getNameOfExpenses());
                quantityInput.setText(newSelection.getQuantity() + "");
                valuePerOneInput.setText(newSelection.getValuePerOne() + "");
            }
        });

        Button addButton = new Button("Add Cost");
        addButton.setOnAction(e -> {
            addButtonClicked(nameInput, quantityInput, valuePerOneInput, typeOfExpense);
            clearButtonClicked(typeOfExpense, nameInput, quantityInput, valuePerOneInput);
        });

        Button deleteButton = new Button("Delete Cost");
        deleteButton.setOnAction(e -> {
            deleteButtonClicked();
            clearButtonClicked(typeOfExpense, nameInput, quantityInput, valuePerOneInput);
        });

        Button clearButton = new Button("Clear");
        clearButton.setOnAction(e -> {
            clearButtonClicked(typeOfExpense, nameInput, quantityInput, valuePerOneInput);
        });

        Button allShopsButton = new Button("All expenses");
        allShopsButton.setMinWidth(150);
        allShopsButton.setOnAction(e -> {
            tableView.getItems().clear();
            tableView.getItems().addAll(getExpenses());
        });

        List<Button> buttonList = buildButtonList(getShops());
        for (Button button : buttonList) {
            button.setMinWidth(150);
            button.setOnAction(e -> {
                this.shopId = controller.getShopByName(button.getText()).getId();
                tableView.getItems().clear();
                tableView.getItems().addAll(controller.getShopByName(button.getText()).getExpenses());
            });
        }

        Label shops = new Label("  Shops");
        shops.setFont(new Font(13));
        shops.setMinWidth(54);
        shops.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        shops.setTextFill(Color.ROYALBLUE);

        HBox buttonsLayout = new HBox();
        buttonsLayout.setSpacing(14);
        buttonsLayout.getChildren().addAll(addButton, deleteButton, clearButton);

        VBox shopVBox = new VBox();
        shopVBox.setSpacing(10);
        shopVBox.getChildren().addAll(shops, allShopsButton);
        shopVBox.getChildren().addAll(buttonList);
        shopVBox.getChildren().addAll(emptySpace, typeRow, nameRow, quantityRow, valuePerOneRow, buttonsLayout);
        shopVBox.setAlignment(Pos.TOP_CENTER);

        HBox hBox = new HBox(tableView, shopVBox);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }

    private void clearButtonClicked(ChoiceBox<String> typeOfExpense, TextField nameInput, TextField quantityInput, TextField valuePerOneInput) {
        nameInput.clear();
        quantityInput.clear();
        valuePerOneInput.clear();
        typeOfExpense.setValue(TypeOfExpense.Product.name());
    }

    private void addButtonClicked(TextField nameInput, TextField quantityInput, TextField valuePerOneInput, ChoiceBox<String> typeOfCost) {
        double totalValue = Integer.parseInt(quantityInput.getText()) * Double.parseDouble(valuePerOneInput.getText());
        Expenses expenses = new Expenses(TypeOfExpense.valueOf(typeOfCost.getValue()), nameInput.getText(), Integer.parseInt(quantityInput.getText()), Double.parseDouble(valuePerOneInput.getText()), totalValue);
        if (shopId == null) {
            AlertBox.displaySimple("Input error!", "Please select a shop.");
            return;
        }
        controller.addExpenses(expenses, shopId);
        tableView.getItems().add(expenses);
    }

    private void deleteButtonClicked() {
        ObservableList<Expenses> allExpenses;
        Expenses expensesSelected;
        allExpenses = tableView.getItems();
        expensesSelected = tableView.getSelectionModel().getSelectedItem();
        controller.deleteExpenseById(expensesSelected.getId());
        allExpenses.remove(expensesSelected);
    }

    private List<Button> buildButtonList(List<Shop> shops) {
        List<Button> buttonsList = new ArrayList<>();
        for (Shop shop : shops) {
            buttonsList.add(new Button(shop.getName()));
        }
        return buttonsList;
    }

    private List<Shop> getShops() {
        List<Shop> shops = new ArrayList<>();
        for (Shop shop : controller.getAllShops()) {
            if (user.getShopIds().contains(shop.getId())){
                shops.add(shop);
            }
        }
        return shops;
    }

    private List<Expenses> getExpenses() {
        List<Expenses> expenses = new ArrayList<>();
        for (Shop shops : controller.getAllShops()) {
                if (user.getShopIds().contains(shops.getId())){
                 expenses.addAll(shops.getExpenses());
                }
        }
        return expenses;
    }


}
