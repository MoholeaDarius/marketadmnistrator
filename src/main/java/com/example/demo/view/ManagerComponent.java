package com.example.demo.view;

import com.example.demo.controller.Controller;
import com.example.demo.model.User;
import com.example.demo.model.UserRole;
import com.example.demo.model.UserStatus;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.ArrayList;
import java.util.List;

public class ManagerComponent {

    private TableView<User> tableView;
    private Controller controller;


    public Node build(Controller controller) {
        tableView = new TableView<>();
        this.controller = controller;
        tableView.setMinHeight(450);
        tableView.setMinWidth(520);
        tableView.setMaxWidth(520);

        TableColumn<User, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setMinWidth(50);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<User, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(100);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<User, String> surnameColumn = new TableColumn<>("Surname");
        surnameColumn.setMinWidth(100);
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));

        TableColumn<User, Integer> cnpColumn = new TableColumn<>("Cnp");
        cnpColumn.setMinWidth(50);
        cnpColumn.setCellValueFactory(new PropertyValueFactory<>("cnp"));

        TableColumn<User, Integer> ageColumn = new TableColumn<>("Age");
        ageColumn.setMinWidth(50);
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));

        TableColumn<User, String> statusColumn = new TableColumn<>("Status");
        statusColumn.setMinWidth(100);
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));

        TableColumn<User, List<Integer>> shopIdsColumn = new TableColumn<>("Shop ids");
        shopIdsColumn.setMinWidth(50);
        shopIdsColumn.setCellValueFactory(new PropertyValueFactory<>("shopIds"));

        tableView.getItems().addAll(controller.getAllManagers());
        tableView.getColumns().addAll(idColumn, nameColumn, surnameColumn, cnpColumn, ageColumn, statusColumn, shopIdsColumn);

        TextField nameInput = new TextField();
        nameInput.setPromptText("name");
        nameInput.setMinWidth(120);

        TextField surnameInput = new TextField();
        surnameInput.setPromptText("surname");
        surnameInput.setMinWidth(120);

        TextField cnpInput = new TextField();
        cnpInput.setPromptText("cnp");
        cnpInput.setMinWidth(120);

        TextField ageInput = new TextField();
        ageInput.setPromptText("age");
        ageInput.setMinWidth(120);

        TextField shopIdsInput = new TextField();
        shopIdsInput.setPromptText("shop ids");
        shopIdsInput.setMinWidth(120);

        TextField usernameInput = new TextField();
        usernameInput.setPromptText("username");
        usernameInput.setMinWidth(120);

        TextField passwordInput = new TextField();
        passwordInput.setPromptText("password");
        passwordInput.setMinWidth(120);

        Label userRoleLabel = new Label();

        ChoiceBox<String> userStatusInput = new ChoiceBox<>();
        userStatusInput.getItems().addAll(UserStatus.active.name(), UserStatus.inactive.name(), UserStatus.restricted.name(), UserStatus.disabled.name());
        userStatusInput.setValue(UserStatus.active.name());
        userStatusInput.setMinWidth(100);

        tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                nameInput.setText(newSelection.getName());
                surnameInput.setText(newSelection.getSurname());
                cnpInput.setText(newSelection.getCnp() + "");
                ageInput.setText(newSelection.getAge() + "");
                shopIdsInput.setText(newSelection.getShopIds() + "");
                usernameInput.setText(newSelection.getUsername());
                passwordInput.setText(newSelection.getPassword());
                userRoleLabel.setText(newSelection.getRole());
                userStatusInput.setValue(newSelection.getStatus());
            }
        });

        //butoane
        Button addButton = new Button("Add");
        addButton.setOnAction(e -> {
            addButtonClicked(nameInput, surnameInput, cnpInput, ageInput, shopIdsInput, usernameInput, passwordInput, userStatusInput);
            clearButtonClicked(nameInput, surnameInput, cnpInput, ageInput, shopIdsInput, usernameInput, passwordInput, userRoleLabel, userStatusInput);

        });

        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> deleteButtonClicked());

        Button updateButton = new Button("Update");
        updateButton.setOnAction(e -> {
            updateButtonClicked(nameInput, surnameInput, cnpInput, ageInput, shopIdsInput, usernameInput, passwordInput, userRoleLabel, userStatusInput);
            clearButtonClicked(nameInput, surnameInput, cnpInput, ageInput, shopIdsInput, usernameInput, passwordInput, userRoleLabel, userStatusInput);
        });

        Button clearButton = new Button("Clear");
        clearButton.setOnAction(e -> {
            clearButtonClicked(nameInput, surnameInput, cnpInput, ageInput, shopIdsInput, usernameInput, passwordInput, userRoleLabel, userStatusInput);

        });

        Label options = new Label("  Options");
        options.setFont(new Font(13));
        options.setMinWidth(54);
        options.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        options.setTextFill(Color.ROYALBLUE);


        Label personalData = new Label("Data");
        personalData.setFont(new Font(13));
        personalData.setMinWidth(54);
        personalData.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        personalData.setTextFill(Color.ROYALBLUE);

        Label name = new Label("Name:");
        name.setMinWidth(54);
        HBox nameRow = new HBox();
        nameRow.setSpacing(10);
        nameRow.getChildren().addAll(name, nameInput);

        Label surname = new Label("Surname:");
        surname.setMinWidth(54);
        HBox surnameRow = new HBox();
        surnameRow.setSpacing(10);
        surnameRow.getChildren().addAll(surname, surnameInput);

        Label cnp = new Label("Cnp:");
        cnp.setMinWidth(54);
        HBox cnpRow = new HBox();
        cnpRow.setSpacing(10);
        cnpRow.getChildren().addAll(cnp, cnpInput);

        Label age = new Label("Age:");
        age.setMinWidth(54);
        HBox ageRow = new HBox();
        ageRow.setSpacing(10);
        ageRow.getChildren().addAll(age, ageInput);

        Label shopIds = new Label("Shop ids:");
        shopIds.setMinWidth(54);
        HBox shopIdsRow = new HBox();
        shopIdsRow.setSpacing(10);
        shopIdsRow.getChildren().addAll(shopIds, shopIdsInput);

        Label userData = new Label("User data");
        userData.setFont(new Font(13));
        userData.setMinWidth(54);
        userData.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        userData.setTextFill(Color.ROYALBLUE);

        Label username = new Label("Username:");
        username.setMinWidth(54);
        HBox usernameRow = new HBox();
        usernameRow.setSpacing(10);
        usernameRow.getChildren().addAll(username, usernameInput);

        Label password = new Label("Password:");
        password.setMinWidth(56);
        HBox passwordRow = new HBox();
        passwordRow.setSpacing(10);
        passwordRow.getChildren().addAll(password, passwordInput);

        Label userStatus = new Label("Status:");
        userStatus.setMinWidth(54);
        HBox userStatusRow = new HBox();
        userStatusRow.setSpacing(10);
        userStatusRow.getChildren().addAll(userStatus, userStatusInput);

        VBox layoutForInputs = new VBox();
        layoutForInputs.setPadding(new Insets(10, 10, 10, 10));
        layoutForInputs.setSpacing(10);
        layoutForInputs.getChildren().addAll(personalData, nameRow, surnameRow, cnpRow, ageRow, shopIdsRow, userData, usernameRow, passwordRow, userStatusRow);

        HBox buttonsHRow = new HBox();
        buttonsHRow.setSpacing(14);
        buttonsHRow.getChildren().addAll(addButton, deleteButton, updateButton, clearButton);

        VBox layoutForInputsAndButtons = new VBox();
        layoutForInputsAndButtons.setSpacing(10);
        layoutForInputsAndButtons.getChildren().addAll(layoutForInputs, options, buttonsHRow);


        HBox hBox = new HBox(tableView, layoutForInputsAndButtons);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }

    private void deleteButtonClicked() {
        ObservableList<User> allManagers;
        User managerSelected;
        allManagers = tableView.getItems();
        managerSelected = tableView.getSelectionModel().getSelectedItem();
        controller.deleteUserById(managerSelected.getId());
        allManagers.remove(managerSelected);
    }

    private void updateButtonClicked(TextField nameInput, TextField surnameInput, TextField cnpInput, TextField ageInput, TextField shopIds, TextField usernameInput, TextField passwordInput, Label userRoleLabel, ChoiceBox<String> userStatusInput) {
        User selectedManager = tableView.getSelectionModel().getSelectedItem();
        List<Integer> newShopIds = new ArrayList<>();
        String[] shopIdsArray = shopIds.getText().split(",");
        for (int i = 0; i < shopIdsArray.length; i++) {
            newShopIds.add(Integer.parseInt(shopIdsArray[i]));
        }
        System.out.println("shop ids: " + newShopIds);
        for (User manager : controller.getAllManagers()) {
            if (selectedManager != null && manager.getId() == selectedManager.getId()) {
                manager.setId(selectedManager.getId());
                manager.setName(nameInput.getText());
                manager.setSurname(surnameInput.getText());
                manager.setCnp(Integer.parseInt(cnpInput.getText()));
                manager.setAge(Integer.parseInt(ageInput.getText()));
                manager.setShopIds(newShopIds);
                manager.setUsername(usernameInput.getText());
                manager.setPassword(passwordInput.getText());
                manager.setRole(UserRole.manager.name());
                manager.setStatus(userStatusInput.getValue());
                manager.setProfilePicture("asasasass");
                controller.updateUser(manager);
            }
        }
        tableView.getItems().clear();
        tableView.getItems().addAll(controller.getAllManagers());
    }

    private void clearButtonClicked(TextField nameInput, TextField surnameInput, TextField cnpInput, TextField ageInput, TextField shopIdsInput, TextField usernameInput, TextField passwordInput, Label userRoleLabel, ChoiceBox<String> userStatusInput) {
        nameInput.clear();
        surnameInput.clear();
        cnpInput.clear();
        ageInput.clear();
        shopIdsInput.clear();
        usernameInput.clear();
        passwordInput.clear();
        userStatusInput.setValue("active");
        userRoleLabel.setText(" ");
    }

    private void addButtonClicked(TextField nameInput, TextField surnameInput, TextField cnpInput, TextField ageInput, TextField shopIds, TextField usernameInput, TextField passwordInput, ChoiceBox<String> userStatusInput) {
        if (usernameInput.getText().isEmpty()) {
            AlertBox.displaySimple("InputError", "Please enter a username");
            return;
        }
        for (User user : controller.getAllUsers()) {
            if (usernameInput.getText().equals(user.getUsername())) {
                AlertBox.displaySimple("InputEror", "This username has been taken!");
                return;
            }
        }
        if (passwordInput.getText().isEmpty()) {
            AlertBox.displaySimple("InputError", "Please enter a password");
            return;
        }
        List<Integer> newShopIds = new ArrayList<>();
        String[] shopIdsArray = shopIds.getText().split(",");
        for (int i = 0; i < shopIdsArray.length; i++) {
            newShopIds.add(Integer.parseInt(shopIdsArray[i]));
        }
        User newUser = new User(nameInput.getText(), surnameInput.getText(), Integer.parseInt(cnpInput.getText()), Integer.parseInt(ageInput.getText()), newShopIds, usernameInput.getText(), passwordInput.getText(), UserRole.manager.name(), userStatusInput.getValue(), "asdas");
        controller.addNewUser(newUser);
        tableView.getItems().add(newUser);
    }

}
