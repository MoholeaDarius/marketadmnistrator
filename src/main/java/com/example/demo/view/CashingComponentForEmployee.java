package com.example.demo.view;

import com.example.demo.model.Cashing;
import com.example.demo.model.Shop;
import com.example.demo.model.User;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import com.example.demo.controller.Controller;

import java.util.ArrayList;
import java.util.List;

public class CashingComponentForEmployee {

    private TableView<Cashing> tableView;
    private Controller controller;
    private User user;
    private Integer shopId;


    public Node build(Controller controller, User user) {
        tableView = new TableView<>();
        this.controller = controller;
        this.user = user;
        tableView.setMinHeight(450);
        tableView.setMinWidth(520);

        TableColumn<Cashing, Integer> idProductColumn = new TableColumn<>("Id");
        idProductColumn.setMinWidth(50);
        idProductColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Cashing, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(100);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("nameOfProduct"));

        TableColumn<Cashing, Integer> quantityColumn = new TableColumn<>("Quantity");
        quantityColumn.setMinWidth(50);
        quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        TableColumn<Cashing, Integer> valuePerOneColumn = new TableColumn<>("Value per one");
        valuePerOneColumn.setMinWidth(70);
        valuePerOneColumn.setCellValueFactory(new PropertyValueFactory<>("valuePerOne"));

        TableColumn<Cashing, Integer> totalValueColumn = new TableColumn<>("Total Value");
        totalValueColumn.setMinWidth(70);
        totalValueColumn.setCellValueFactory(new PropertyValueFactory<>("totalValue"));

        tableView.getItems().addAll(getCashingList());
        tableView.getColumns().addAll(idProductColumn, nameColumn, quantityColumn, valuePerOneColumn, totalValueColumn);

        Label name = new Label("Name:");
        name.setMinWidth(56);
        TextField nameInput = new TextField();
        nameInput.setPromptText("Name");
        nameInput.setMinWidth(56);
        nameInput.setEditable(false);
        HBox nameRow = new HBox();
        nameRow.setSpacing(10);
        nameRow.getChildren().addAll(name, nameInput);

        Label quantity = new Label("Quantity:");
        quantity.setMinWidth(56);
        TextField quantityInput = new TextField();
        quantityInput.setPromptText("quantity");
        quantityInput.setMinWidth(56);
        HBox quantityRow = new HBox();
        quantityRow.setSpacing(10);
        quantityRow.getChildren().addAll(quantity, quantityInput);

        Label valuePerOne = new Label("Price:");
        valuePerOne.setMinWidth(56);
        TextField valuePerOneInput = new TextField();
        valuePerOneInput.setPromptText("value per one");
        valuePerOneInput.setMinWidth(56);
        HBox valuePerOneRow = new HBox();
        valuePerOneRow.setSpacing(10);
        valuePerOneRow.getChildren().addAll(valuePerOne, valuePerOneInput);

        Label shops = new Label("  Shops");
        shops.setFont(new Font(13));
        shops.setMinWidth(54);
        shops.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        shops.setTextFill(Color.ROYALBLUE);

        tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                nameInput.setText(newSelection.getNameOfProduct());
                quantityInput.setText(newSelection.getQuantity() + "");
                valuePerOneInput.setText(newSelection.getValuePerOne() + "");
            }
        });

        Label emptySpace = new Label("");

        Button allShopsButton = new Button("All chasing");
        allShopsButton.setMinWidth(150);
        allShopsButton.setOnAction(e -> {
            tableView.getItems().clear();
            tableView.getItems().addAll(getCashingList());
        });

        Button addButton = new Button("Add Cost");
        addButton.setOnAction(e -> {
            addButtonClicked(nameInput, quantityInput, valuePerOneInput);
            clearButtonClicked(nameInput, quantityInput, valuePerOneInput);
        });

        Button deleteButton = new Button("Delete Cost");
        deleteButton.setOnAction(e -> {
            deleteButtonClicked();
            clearButtonClicked(nameInput, quantityInput, valuePerOneInput);
        });

        Button clearButton = new Button("Clear");
        clearButton.setOnAction(e -> {
            clearButtonClicked(nameInput, quantityInput, valuePerOneInput);
        });

        List<Button> buttonList = buildButtonList(getShops());
        for (Button button : buttonList) {
            button.setMinWidth(150);
            button.setOnAction(e -> {
                this.shopId = controller.getShopByName(button.getText()).getId();
                tableView.getItems().clear();
                tableView.getItems().addAll(controller.getShopByName(button.getText()).getCashingList());
            });
        }

        HBox buttonsLayout = new HBox();
        buttonsLayout.setSpacing(14);
        buttonsLayout.getChildren().addAll(addButton, deleteButton, clearButton);

        VBox inputLayout = new VBox();
        inputLayout.getChildren().addAll(shops, allShopsButton);
        inputLayout.getChildren().addAll(buttonList);
        inputLayout.getChildren().addAll(emptySpace, nameRow, quantityRow, valuePerOneRow, buttonsLayout);
        inputLayout.setSpacing(10);
        inputLayout.setAlignment(Pos.TOP_CENTER);

        HBox hBox = new HBox(tableView, inputLayout);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }

    private void deleteButtonClicked() {
        ObservableList<Cashing> cashingList;
        Cashing expensesSelected;
        cashingList = tableView.getItems();
        expensesSelected = tableView.getSelectionModel().getSelectedItem();
        controller.deleteExpenseById(expensesSelected.getId());
        cashingList.remove(expensesSelected);
    }

    private void addButtonClicked(TextField nameInput, TextField quantityInput, TextField valuePerOneInput) {
        double totalValue = Integer.parseInt(quantityInput.getText()) * Double.parseDouble(valuePerOneInput.getText());
        Cashing cashing = new Cashing(nameInput.getText(), Integer.parseInt(quantityInput.getText()), Double.parseDouble(valuePerOneInput.getText()), totalValue);
        if (shopId == null) {
            AlertBox.displaySimple("Input error!", "Please select a shop.");
            return;
        }
        controller.addCashing(cashing, shopId);
        tableView.getItems().add(cashing);
    }

    private void clearButtonClicked(TextField nameInput, TextField quantityInput, TextField valuePerOneInput) {
        nameInput.clear();
        quantityInput.clear();
        valuePerOneInput.clear();
    }

    private List<Button> buildButtonList(List<Shop> shops) {
        List<Button> buttonsList = new ArrayList<>();
        for (Shop shop : shops){
            buttonsList.add(new Button(shop.getName()));
        }
        return buttonsList;
    }

    private List<Shop> getShops() {
        List<Shop> shops = new ArrayList<>();
        for (Shop shop : controller.getAllShops()) {
            if (user.getShopIds().contains(shop.getId())){
                shops.add(shop);
            }
        }
        return shops;
    }

    private List<Cashing> getCashingList() {
        List<Cashing> cashing = new ArrayList<>();
        for (Shop shops : controller.getAllShops()) {
            if (user.getShopIds().contains(shops.getId())){
                cashing.addAll(shops.getCashingList());
            }
        }
        return cashing;
    }

}
