package com.example.demo.view;

import com.example.demo.Main;
import com.example.demo.controller.Controller;
import com.example.demo.model.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.IOException;

public class AdminMainView extends Parent {

    private BorderPane borderPane;
    private User user;
    private final Controller controller;


    public AdminMainView(Controller controller, User user) {
        this.controller = controller;
        this.user = user;
        getChildren().add(buildContentForAdmin());
    }

    private Parent buildContentForAdmin() {
        borderPane = new BorderPane();
        borderPane.setTop(buildTop());
        borderPane.setLeft(buildLeft());
        borderPane.setCenter(new HomeComponentUser().build(controller, user));
        borderPane.setBottom(buildBottom());
        borderPane.getLeft().setStyle("-fx-border-color : gray; -fx-border-width : 0 1 ");
        return borderPane;
    }

    private Node buildLeft() {
        Label options = new Label("Options");
        options.setFont(new Font(14));
        options.setAlignment(Pos.CENTER);
        options.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        options.setTextFill(Color.ROYALBLUE);

        Button homeButton = new Button("Home");
        homeButton.setOnAction(e -> {
            borderPane.setCenter(new HomeComponentUser().build(controller, user));
        });
        homeButton.setMinWidth(80);
        homeButton.setAlignment(Pos.CENTER);

        Button shopButton = new Button("Shop");
        shopButton.setOnAction(e -> {
            borderPane.setCenter(new ShopsComponent().build(controller));
        });
        shopButton.setMinWidth(80);
        shopButton.setAlignment(Pos.CENTER);

        Button managerButton = new Button("Managers");
        managerButton.setOnAction(e -> {
            borderPane.setCenter(new ManagerComponent().build(controller));
        });
        managerButton.setMinWidth(80);
        managerButton.setAlignment(Pos.CENTER);

        Button cashingButton = new Button("Cashing");
        cashingButton.setMinWidth(80);
        cashingButton.setOnAction(e ->{
            borderPane.setCenter(new CashingComponentForAdmin().build(controller));
        });
        cashingButton.setAlignment(Pos.CENTER);

        Button expensesButton = new Button("Expense");
        expensesButton.setMinWidth(80);
        expensesButton.setOnAction(e ->{
            borderPane.setCenter(new ExpensesComponentForAdmin().build(controller));
        });
        expensesButton.setAlignment(Pos.CENTER);

        VBox vBox = new VBox(options, homeButton, shopButton, managerButton, cashingButton, expensesButton);
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(10, 10, 10, 10));
        return vBox;
    }

    private Node buildBottom() {
        Label bottom = new Label(" Username: " + user.getUsername() + "  |  User role: " + user.getRole() + "  |  User status: " + user.getStatus());
        HBox hBox = new HBox(1050);
        hBox.getChildren().add(bottom);
        hBox.setSpacing(10);
        hBox.setStyle("-fx-border-color : gray");
        hBox.setAlignment(Pos.BOTTOM_LEFT);
        return hBox;
    }

    private Node buildTop() {
        Menu fileMenu = new Menu("File");
        MenuItem logOutFile = new MenuItem("Log Out");
        logOutFile.setOnAction(e -> {
            try {
                logOutOptions();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        MenuItem exitFile = new MenuItem("Exit");
        exitFile.setOnAction(e -> closeProgram());

        Main.window.setOnCloseRequest(e -> {
            e.consume();
            closeProgram();
        });

        fileMenu.getItems().add(logOutFile);
        fileMenu.getItems().add(exitFile);


        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu);
        return menuBar;
    }

    private void closeProgram() {
        Boolean answer = ConfirmBox.displaySimple("Exit tab", "Are you sure you want close the app?");
        if (answer) {
            Main.window.close();
        }
    }

    private void logOutOptions() throws IOException {
        Boolean answer = ConfirmBox.displaySimple("LogOut", "Are you sure you want to logout?");
        if (answer) {
            Main.window.setScene(new Scene(new LoginView(controller), 280, 300));
        }
    }


}

