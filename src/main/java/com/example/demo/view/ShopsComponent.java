package com.example.demo.view;

import com.example.demo.controller.Controller;
import com.example.demo.model.Shop;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ShopsComponent {

    private TableView<Shop> tableView;
    private Controller controller;

    public Node build(Controller controller) {
        tableView = new TableView<>();
        this.controller = controller;
        tableView.setMinHeight(450);
        tableView.setMinWidth(520);
        tableView.setMaxWidth(520);

        TableColumn<Shop, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setMinWidth(50);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Shop, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(150);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Shop, String> addressColumn = new TableColumn<>("Address");
        addressColumn.setMinWidth(120);
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));

        tableView.getItems().addAll(controller.getCompany().getShops());
        tableView.getColumns().addAll(idColumn, nameColumn, addressColumn);

        TextField nameInput = new TextField();
        nameInput.setPromptText("name");
        nameInput.setMinWidth(120);

        TextField addressInput = new TextField();
        addressInput.setPromptText("address");
        addressInput.setMinWidth(120);

        tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                nameInput.setText(newSelection.getName());
                addressInput.setText(newSelection.getAddress());
            }
        });

        Button addButton = new Button("Add");
        addButton.setOnAction(e -> {
            addButtonClicked(nameInput, addressInput);
            clearButtonClicked(nameInput, addressInput);
        });

        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> deleteButtonClicked());

        Button updateButton = new Button("Update");
        updateButton.setOnAction(e -> {
            updateButtonClicked(nameInput, addressInput);
            clearButtonClicked(nameInput, addressInput);
        });

        Button clearButton = new Button("Clear");
        clearButton.setOnAction(e -> {
            clearButtonClicked(nameInput, addressInput);
        });

        Label personalData = new Label("Data");
        personalData.setFont(new Font(13));
        personalData.setMinWidth(54);
        personalData.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        personalData.setTextFill(Color.ROYALBLUE);

        Label name = new Label("Name:");
        name.setMinWidth(54);
        HBox nameRow = new HBox();
        nameRow.setSpacing(10);
        nameRow.getChildren().addAll(name, nameInput);

        Label address = new Label("Address:");
        address.setMinWidth(54);
        HBox addressRow = new HBox();
        addressRow.setSpacing(10);
        addressRow.getChildren().addAll(address, addressInput);

        Label options = new Label("  Options");
        options.setFont(new Font(13));
        options.setMinWidth(54);
        options.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        options.setTextFill(Color.ROYALBLUE);

        VBox layoutForInputs = new VBox();
        layoutForInputs.setPadding(new Insets(10, 10, 10, 10));
        layoutForInputs.setSpacing(10);
        layoutForInputs.getChildren().addAll(personalData, nameRow, addressRow);

        HBox buttonsHRow = new HBox();
        buttonsHRow.setSpacing(14);
        buttonsHRow.getChildren().addAll(addButton, deleteButton, updateButton, clearButton);

        VBox layoutForInputsAndButtons = new VBox();
        layoutForInputsAndButtons.setSpacing(10);
        layoutForInputsAndButtons.getChildren().addAll(layoutForInputs, options, buttonsHRow);

        HBox hBox = new HBox(tableView, layoutForInputsAndButtons);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }

    private void updateButtonClicked(TextField nameInput, TextField addressInput) {
        for (Shop shop : controller.getCompany().getShops()) {
            if (nameInput.getText().equals(shop.getName())) {
                AlertBox.displaySimple("InputError", "This username has been taken!");
                return;
            }
        }
        Shop selectedShop = tableView.getSelectionModel().getSelectedItem();
        tableView.getItems().removeAll(controller.getCompany().getShops());
        for (Shop shop : controller.getCompany().getShops()) {
            if (selectedShop != null && shop.getId() == selectedShop.getId()) {
                shop.setId(selectedShop.getId());
                shop.setName(nameInput.getText());
                shop.setAddress(addressInput.getText());
                controller.updateShop(shop);
            }
        }
        tableView.getItems().clear();
        tableView.getItems().addAll(controller.getCompany().getShops());
    }

    private void deleteButtonClicked() {
        ObservableList<Shop> allShops;
        Shop shop;
        allShops = tableView.getItems();
        shop = tableView.getSelectionModel().getSelectedItem();
        controller.deleteShopById(shop.getId());
        allShops.remove(shop);
    }

    private void clearButtonClicked(TextField nameInput, TextField addressInput) {
        nameInput.clear();
        addressInput.clear();
    }

    private void addButtonClicked(TextField nameInput, TextField addressInput) {
        for (Shop shop : controller.getCompany().getShops()) {
            if (nameInput.getText().equals(shop.getName())) {
                AlertBox.displaySimple("InputError", "This username has been taken!");
                return;
            }
        }
        Shop shop = new Shop(nameInput.getText(), addressInput.getText());
        controller.addNewShop(shop);
        tableView.getItems().addAll(shop);
    }

}
