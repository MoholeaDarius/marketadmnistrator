package com.example.demo.view;

import com.example.demo.Main;
import com.example.demo.model.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import com.example.demo.controller.Controller;

import java.io.IOException;

public class EmployeeMainView extends Parent {

    private BorderPane borderPane;
    private Controller controller;
    private User user;

    public EmployeeMainView(Controller controller, User user) {
        this.controller = controller;
        this.user = user;
        getChildren().add(buildContentForManager());
    }

    private Node buildContentForManager() {
        borderPane = new BorderPane();
        borderPane.setTop(buildTop());
        borderPane.setLeft(buildLeft());
        borderPane.setCenter(new HomeComponentUser().build(controller, user));
        borderPane.setBottom(buildBottom());
        borderPane.getLeft().setStyle("-fx-border-color : gray; -fx-border-width : 0 1 ");
        return borderPane;
    }

    private Node buildBottom() {
//        Label bottom = new Label(" Username: " + employee.getEmployeeAccount().getUsername() + "  |  User role: " + employee.getEmployeeAccount().getRole() + "  |  User status: " + employee.getEmployeeAccount().getStatus());
        HBox hBox = new HBox(1050);
//        hBox.getChildren().add(bottom);
        hBox.setSpacing(10);
        hBox.setStyle("-fx-border-color : gray");
        hBox.setAlignment(Pos.BOTTOM_LEFT);
        return hBox;
    }

    private Node buildLeft() {
        Label options = new Label("Options");
        options.setFont(new Font(14));
        options.setAlignment(Pos.CENTER);
        options.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        options.setTextFill(Color.ROYALBLUE);

        Button homeButton = new Button("Home");
        homeButton.setOnAction(e -> borderPane.setCenter(new HomeComponentUser().build(controller, user)));
        homeButton.setMinWidth(80);
        homeButton.setAlignment(Pos.CENTER);

        Button cashingButton = new Button("Cashing");
        cashingButton.setOnAction(e -> borderPane.setCenter(new CashingComponentForEmployee().build(controller, user)));
        cashingButton.setMinWidth(80);
        cashingButton.setAlignment(Pos.CENTER);

        Button productButton = new Button("products");
        productButton.setOnAction(e -> borderPane.setCenter(new ProductsComponent().build(controller, user)));
        productButton.setMinWidth(80);
        productButton.setAlignment(Pos.CENTER);

        VBox vBox = new VBox(options, homeButton, cashingButton, productButton);
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(10, 10, 10, 10));
        return vBox;
    }

    private Node buildTop() {
        Menu fileMenu = new Menu("File");
        MenuItem logOutFile = new MenuItem("Log Out");
        logOutFile.setOnAction(e -> {
            try {
                logOutOptions();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        Main.window.setOnCloseRequest(e -> {
            e.consume();
            closeProgram();
        });

        fileMenu.getItems().add(logOutFile);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu);
        return menuBar;
    }

    private void closeProgram() {
        Boolean answer = ConfirmBox.displaySimple("Exit tab", "Are you sure you want close the app?");
        if (answer) {
            Main.window.close();
        }
    }

    private void logOutOptions() throws IOException {
        Boolean answer = ConfirmBox.displaySimple("LogOut", "Are you sure you want to logout?");
        if (answer) {
            Main.window.setScene(new Scene(new LoginView(controller), 280, 300));
        }
    }

}
