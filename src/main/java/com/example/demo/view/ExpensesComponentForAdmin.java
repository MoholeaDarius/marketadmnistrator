package com.example.demo.view;

import com.example.demo.controller.Controller;
import com.example.demo.model.Expenses;
import com.example.demo.model.Shop;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ExpensesComponentForAdmin {

    private TableView<Expenses> tableView;
    private Controller controller;


    public Node build(Controller controller) {
        tableView = new TableView<>();
        this.controller = controller;
        tableView.setMinHeight(450);
        tableView.setMinWidth(520);

        TableColumn<Expenses, Integer> idCostColumn = new TableColumn<>("Id");
        idCostColumn.setMinWidth(50);
        idCostColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Expenses, String> typeCostColumn = new TableColumn<>("Name of expense");
        typeCostColumn.setMinWidth(75);
        typeCostColumn.setCellValueFactory(new PropertyValueFactory<>("nameOfExpenses"));

        TableColumn<Expenses, String> nameCostColumn = new TableColumn<>("Type of cost");
        nameCostColumn.setMinWidth(50);
        nameCostColumn.setCellValueFactory(new PropertyValueFactory<>("typeOfExpenses"));

        TableColumn<Expenses, Integer> quantityCostColumn = new TableColumn<>("Quantity");
        quantityCostColumn.setMinWidth(50);
        quantityCostColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        TableColumn<Expenses, Double> valuePerOneCostColumn = new TableColumn<>("Value per one");
        valuePerOneCostColumn.setMinWidth(50);
        valuePerOneCostColumn.setCellValueFactory(new PropertyValueFactory<>("valuePerOne"));

        TableColumn<Expenses, Double> totalValueCostColumn = new TableColumn<>("Total value");
        totalValueCostColumn.setMinWidth(60);
        totalValueCostColumn.setCellValueFactory(new PropertyValueFactory<>("totalValue"));

        tableView.getColumns().addAll(idCostColumn, typeCostColumn, nameCostColumn, quantityCostColumn, valuePerOneCostColumn, totalValueCostColumn);
        tableView.getItems().addAll(controller.getAllExpenses());

        Button allShopsButton = new Button("All expenses");
        allShopsButton.setMinWidth(150);
        allShopsButton.setOnAction(e -> {
            tableView.getItems().clear();
            tableView.getItems().addAll(controller.getAllExpenses());
        });

        List<Button> buttonList = buildButtonList(controller.getCompany().getShops());
        for (Button button : buttonList) {
            button.setMinWidth(150);
            button.setOnAction(e -> {
                tableView.getItems().clear();
                tableView.getItems().addAll(controller.getShopByName(button.getText()).getExpenses());
            });
        }

        Label shops = new Label("  Shops");
        shops.setFont(new Font(13));
        shops.setMinWidth(54);
        shops.setFont(Font.font("Lucida Sans Unicode", FontWeight.MEDIUM, 15));
        shops.setTextFill(Color.ROYALBLUE);

        InputStream stream = null;
        try {
            stream = new FileInputStream("C:\\Users\\Moholea\\Pictures\\reclamaJavaFx2.png");
        } catch (FileNotFoundException e) {
        }
        Image image = new Image(stream);
        ImageView imageView = new ImageView();
        imageView.setImage(image);
        imageView.setX(10);
        imageView.setY(10);
        imageView.setPreserveRatio(true);

        VBox shopVBox = new VBox();
        shopVBox.setSpacing(10);
        shopVBox.getChildren().addAll(shops, allShopsButton);
        shopVBox.getChildren().addAll(buttonList);
        shopVBox.setAlignment(Pos.TOP_CENTER);

        HBox hBox = new HBox(tableView, shopVBox, imageView);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        return hBox;
    }

    private List<Button> buildButtonList(List<Shop> shops) {
        List<Button> buttonsList = new ArrayList<>();
        for (Shop shop : shops){
            buttonsList.add(new Button(shop.getName()));
        }
        return buttonsList;
    }

}
