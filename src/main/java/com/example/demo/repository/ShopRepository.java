package com.example.demo.repository;

import com.example.demo.model.*;
import com.example.demo.repository.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShopRepository {

    private ProductRepository productRepository;
    private ExpensesRepository expensesRepository;
    private CashingRepository cashingRepository;
    private UserRepository userRepository;

    public ShopRepository(ProductRepository productRepository, ExpensesRepository expensesRepository, CashingRepository cashingRepository, UserRepository userRepository) {
        this.productRepository = productRepository;
        this.expensesRepository = expensesRepository;
        this.cashingRepository = cashingRepository;
        this.userRepository = userRepository;
    }

    public List<Shop> findAll() {
        List<Shop> shops = new ArrayList<>();
        String sql = "SELECT * FROM shop";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                List<Product> products = productRepository.findProductsByShopId(id);
                List<Expenses> expenses = expensesRepository.findExpensesByShopId(id);
                List<Cashing> cashingList = cashingRepository.findCashingByShopId(id);
                List<User> users = userRepository.findUsersByShopId(id);
                shops.add(new Shop(id, name, address, products, expenses, cashingList, users));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return shops;
    }

    public void insert(Shop shop){
        int companyId = 1;
        shop.setId(setId());
        String sql = "insert into shop (id, name, address, company_id) values (?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, shop.getId());
            statement.setString(2, shop.getName());
            statement.setString(3, shop.getAddress());
            statement.setInt(4, companyId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void update(Shop shop) {
        String sql = "update shop set id = ?, name = ?, address = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, shop.getId());
            statement.setString(2, shop.getName());
            statement.setString(3, shop.getAddress());
            statement.setInt(4, shop.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteById(int id){
        String sql = "delete from shop where id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            userRepository.deleteFromRouterByShopId(id);
            productRepository.deleteProductByShopId(id);
            expensesRepository.deleteExpenseByShopId(id);
            cashingRepository.deleteCashingByShopId(id);
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public Shop findById(int id) {
        String sql = "select * from shop where id = " + id;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()){
            resultSet.next();
            int returnedId = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String address = resultSet.getString("address");
            List<Product> products = productRepository.findProductsByShopId(id);
            List<Expenses> expenses = expensesRepository.findExpensesByShopId(id);
            List<Cashing> cashingList = cashingRepository.findCashingByShopId(id);
            List<User> users = userRepository.findUsersByShopId(returnedId);
            return new Shop(returnedId, name,address,products,expenses,cashingList,users);
        }catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public Shop getShopByName(String shopName){
        for (Shop shop : findAll()){
            if (shop.getName().equals(shopName)){
                return shop;
            }
        }
        return null;
    }

    /**
     * Creates a unique id.
     *
     * @return id
     */
    private int setId() {
        if (findAll().isEmpty()) return 1;
        else return findAll().get(findAll().size() - 1).getId() + 1;
    }

}
