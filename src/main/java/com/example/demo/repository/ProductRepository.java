package com.example.demo.repository;

import com.example.demo.model.Product;
import com.example.demo.model.TypeOfExpense;
import com.example.demo.repository.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductRepository {

    /**
     * Create a list with every product in the app.
     *
     * @return product
     */
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        String sql = "select * from product";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                double price = resultSet.getDouble("price");
                int quantity = resultSet.getInt("quantity");
                products.add(new Product(id, name, price, quantity));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return products;
    }

    /**
     * Find a product from the id you give.
     *
     * @param id
     * @return
     */
    public Product findById(int id) {
        String sql = "select * from product where id = " + id;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
            int returnedId = resultSet.getInt("id");
            String name = resultSet.getString("name");
            double price = resultSet.getDouble("price");
            int quantity = resultSet.getInt("quantity");
            return new Product(returnedId, name, price, quantity);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    /**
     * insert a product in product list
     *
     * @param newProduct
     * @param shopId
     */
    public void insert(Product newProduct, int shopId) {
        for (Product product : findProductsByShopId(shopId)) {
            if (product.getName().equals(newProduct.getName())) {
                newProduct.setId(product.getId());
                update(newProduct);
                addExpense(newProduct, shopId);
                return;
            }
        }
        String sql = "insert into product (id, name, price, quantity, shop_id) values (?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, setId());
            statement.setString(2, newProduct.getName());
            statement.setDouble(3, newProduct.getPrice());
            statement.setInt(4, newProduct.getQuantity());
            statement.setInt(5, shopId);
            statement.execute();
            addExpense(newProduct, shopId);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * update a product
     *
     * @param product
     */
    public void update(Product product) {
        String sql = "update product set id = ? name = ? price = ? quantity = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, product.getId());
            statement.setString(2, product.getName());
            statement.setDouble(3, product.getPrice());
            statement.setInt(4, product.getQuantity());
            statement.setInt(5, product.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Delete a product from the id you give.
     *
     * @param id
     */
    public void deleteById(int id) {
        String sql = "delete from product where id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteProductByShopId(int shopId) {
        String sql = "delete from product where shop_id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, shopId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Finds you the product list from the shop id you give.
     *
     * @param shopId
     * @return
     */
    public List<Product> findProductsByShopId(int shopId) {
        List<Product> products = new ArrayList<>();
        String sql = "select * from product where shop_id = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                double price = resultSet.getDouble("price");
                int quantity = resultSet.getInt("quantity");
                products.add(new Product(id, name, price, quantity));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return products;
    }

    public Product getProductByName(String productName) {
        for (Product product : findAll()) {
            if (product.getName().equals(productName)) {
                return product;
            }
        }
        return null;
    }

    public void addExpense(Product product, int shopId) {
        String sql = "insert into expense (id, type, name_of_expenses, quantity, value_per_one, total_value, shop_id) values (?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, setIdForExpense());
            statement.setString(2, TypeOfExpense.Product.name());
            statement.setString(3, product.getName());
            statement.setInt(4, product.getQuantity());
            statement.setDouble(5, product.getPrice());
            statement.setDouble(6, product.getQuantity() * product.getPrice());
            statement.setInt(7, shopId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

    }

    public int setIdForExpense() {
        List<Integer> ids = new ArrayList<>();
        String sql = "select * from expense";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                ids.add(id);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        if (ids.isEmpty()) return 1;
        else return ids.get(ids.size() - 1) + 1;
    }

    /**
     * Creates a unique id.
     *
     * @return id
     */
    private int setId() {
        if (findAll().isEmpty()) return 1;
        else return findAll().get(findAll().size() - 1).getId() + 1;
    }

}