package com.example.demo.repository;

import com.example.demo.model.Cashing;
import com.example.demo.model.Product;
import com.example.demo.repository.util.DBConnection;
import com.example.demo.view.AlertBox;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CashingRepository {

    /**
     * Create a list with every cashing in the app.
     *
     * @return cashing
     */
    public List<Cashing> findAll() {
        List<Cashing> cashing = new ArrayList<>();
        String sql = "select * from cashing";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String nameOfProduct = resultSet.getString("name");
                int quantity = resultSet.getInt("quantity");
                double valuePerOne = resultSet.getDouble("value_per_one");
                double totalValue = resultSet.getDouble("total_value");
                cashing.add(new Cashing(id, nameOfProduct, quantity, valuePerOne, totalValue));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return cashing;
    }

    /**
     * Find a cashing from the id you give.
     *
     * @param id is a cashing id
     * @return
     */
    public Cashing findById(int id) {
        String sql = "select * from cashing where id = " + id;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
            int returnedId = resultSet.getInt("id");
            String nameOfProduct = resultSet.getString("name");
            int quantity = resultSet.getInt("quantity");
            double valuePerOne = resultSet.getDouble("value_per_one");
            double totalValue = resultSet.getDouble("total_value");
            return new Cashing(returnedId, nameOfProduct, quantity, valuePerOne, totalValue);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    /**
     * insert a cashing in cashing list
     *
     * @param cashing is the cashing you want to insert
     * @param shopId
     */
    public void insert(Cashing cashing, int shopId) {
        if (!checkTheWarehouse(cashing)){
            AlertBox.displaySimple("InputError","Wrong quantity!");
            return;
        }
        cashing.setId(setId());
        String sql = "insert into cashing (id, name, quantity, value_per_one, total_value, shop_id) values (?, ?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, cashing.getId());
            statement.setString(2, cashing.getNameOfProduct());
            statement.setInt(3, cashing.getQuantity());
            statement.setDouble(4, cashing.getValuePerOne());
            statement.setDouble(5, cashing.getTotalValue());
            statement.setInt(6, shopId);
            statement.execute();
            updateProduct(cashing);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private boolean checkTheWarehouse(Cashing cashing) {
            String sql = "select * from product where name = " + cashing.getNameOfProduct();
            try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Product product = new Product(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getDouble("price"), resultSet.getInt("quantity"));
                    if (product.getQuantity()<cashing.getQuantity()){
                        return false;
                    }
                }
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
            }
            return true;
    }

    private void updateProduct(Cashing cashing) {
        Product product = findProductByName(cashing.getNameOfProduct());
        String sql = "update product set id = ? name = ? price = ? quantity = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, product.getId());
            statement.setString(2, cashing.getNameOfProduct());
            statement.setDouble(3, cashing.getTotalValue());
            statement.setInt(4, product.getQuantity() - cashing.getQuantity());
            statement.setInt(5, product.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public List<Product> findAllProducts() {
        List<Product> products = new ArrayList<>();
        String sql = "select * from product";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                double price = resultSet.getDouble("price");
                int quantity = resultSet.getInt("quantity");
                products.add(new Product(id, name, price, quantity));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return products;
    }

    private Product findProductByName(String nameOfProduct) {
        for (Product product : findAllProducts()){
            if(product.getName().equals(nameOfProduct)){
                return product;
            }
        }
        return null;
    }

    /**
     * update a cashing
     *
     * @param cashing
     */
    public void update(Cashing cashing) {
        String sql = "update cashing set id = ? name = ? quantity = ? value_per_one = ? total_value = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, cashing.getId());
            statement.setString(2, cashing.getNameOfProduct());
            statement.setInt(3, cashing.getQuantity());
            statement.setDouble(4, cashing.getValuePerOne());
            statement.setDouble(5, cashing.getTotalValue());
            statement.setInt(6, cashing.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Delete a cashing from the id you give.
     *
     * @param id
     */
    public void deleteById(int id) {
        String sql = "delete from chasing where id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteCashingByShopId(int id) {
        String sql = "delete from cashing where shop_id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Finds you the cashing list from the shop id you give.
     *
     * @param shopId
     * @return
     */
    public List<Cashing> findCashingByShopId(int shopId) {
        List<Cashing> cashing = new ArrayList<>();
        String sql = "select * from cashing where shop_id = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String nameOfProduct = resultSet.getString("name");
                int quantity = resultSet.getInt("quantity");
                double valuePerOne = resultSet.getDouble("value_per_one");
                double totalValue = resultSet.getDouble("total_value");
                cashing.add(new Cashing(id, nameOfProduct, quantity, valuePerOne, totalValue));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return cashing;
    }

    /**
     * Creates a unique id.
     *
     * @return id
     */
    private int setId() {
        if (findAll().isEmpty()) return 1;
        else return findAll().get(findAll().size() - 1).getId() + 1;
    }

}
