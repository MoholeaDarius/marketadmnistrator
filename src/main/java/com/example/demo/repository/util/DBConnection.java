package com.example.demo.repository.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private String URL = "jdbc:mysql://127.0.0.1:3306/store_management";
    private String USER = "MoholeaDarius";
    private String PASSWORD = "hCKsaqS6";
    private static DBConnection dbConnection;

    private DBConnection() {
    }

    public static DBConnection getInstance() {
        if (dbConnection == null) {
            dbConnection = new DBConnection();
        }

        return dbConnection;
    }

    public Connection get() throws SQLException {
        return DriverManager.getConnection(this.URL, this.USER, this.PASSWORD);
    }
}
