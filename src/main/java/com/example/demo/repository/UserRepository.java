package com.example.demo.repository;

import com.example.demo.model.User;
import com.example.demo.repository.util.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    /**
     * Create a list with every user in the app.
     *
     * @return
     */
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        String sql = "select * from user ";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                int cnp = resultSet.getInt("cnp");
                int age = resultSet.getInt("age");
                List<Integer> shopId = findShopIdFromUserByUserId(resultSet.getInt("id"));
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                String role = resultSet.getString("role");
                String status = resultSet.getString("status");
                String profilePicture = resultSet.getString("profile_picture");
                users.add(new User(id, name, surname, cnp, age, shopId, username, password, role, status, profilePicture));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return users;
    }

    public List<User> findUsersByShopId(int id) {
        List<User> users = new ArrayList<>();
        String sql = "select user_id from user_shop where shop_id = " + id;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                users.add(findById(resultSet.getInt("user_id")));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return users;
    }

    public List<Integer> findShopIdFromUserByUserId(int id) {
        List<Integer> shopIds = new ArrayList<>();
        String sql = "select * from user_shop where user_id = " + id;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                shopIds.add(resultSet.getInt("shop_id"));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return shopIds;
    }

    /**
     * Find a user from the id you give.
     *
     * @param
     * @return
     */
    public User findById(int id) {
        String sql = "select * from user where id = " + id;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
            int returnedId = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String surname = resultSet.getString("surname");
            int cnp = resultSet.getInt("cnp");
            int age = resultSet.getInt("age");
            List<Integer> shopIds = findShopIdFromUserByUserId(id);
            String username = resultSet.getString("username");
            String password = resultSet.getString("password");
            String role = resultSet.getString("role");
            String status = resultSet.getString("status");
            String profilePicture = resultSet.getString("profile_picture");
            return new User(returnedId, name, surname, cnp, age, shopIds, username, password, role, status, profilePicture);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    /**
     * insert a user in users.
     *
     * @param user
     */
    public void insert(User user) {
        user.setId(setId());
        String sql = "insert into user (id, name, surname, cnp, age, username, password, role, status, profile_picture) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, user.getId());
            statement.setString(2, user.getName());
            statement.setString(3, user.getSurname());
            statement.setInt(4, user.getCnp());
            statement.setInt(5, user.getAge());
            statement.setString(6, user.getUsername());
            statement.setString(7, user.getPassword());
            statement.setString(8, user.getRole());
            statement.setString(9, user.getStatus());
            statement.setString(10, user.getProfilePicture());
            statement.execute();
            insertInRouter(user);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private void insertInRouter(User user) {
        for (Integer shopId : user.getShopIds()) {
            insertUserShopInSql(user.getId(), shopId);
        }
    }

    public void insertUserShopInSql(int userId, int shopId) {
        String sql = "insert into user_shop (id, user_id, shop_id) values (?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, setIdForUserShop());
            statement.setInt(2, userId);
            statement.setInt(3, shopId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public List<Integer> findAllIdsForUserShop() {
        List<Integer> ids = new ArrayList<>();
        String sql = "select id from user_shop ";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                ids.add(id);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return ids;
    }

    /**
     * Delete a user from the id you give.
     *
     * @param id
     */
    public void deleteById(int id) {
        String sql = "delete from user where id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            for (Integer shopId : findById(id).getShopIds()) {
                deleteFromRouterByUserId(id);
            }
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private void deleteFromRouterByUserId(int userId) {
        String sql = "delete from user_shop where user_id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, userId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteFromRouterByShopId(int shopId) {
        String sql = "delete from user_shop where shop_id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, shopId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * update a user
     *
     * @param
     */
    public void update(User user) {
        String sql = "update user set id = ?, name = ?, surname = ?, cnp = ?, age = ?, username = ?, password = ?, role = ?, status = ?, profile_picture = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, user.getId());
            statement.setString(2, user.getName());
            statement.setString(3, user.getSurname());
            statement.setInt(4, user.getCnp());
            statement.setInt(5, user.getAge());
            statement.setString(6, user.getUsername());
            statement.setString(7, user.getPassword());
            statement.setString(8, user.getRole());
            statement.setString(9, user.getStatus());
            statement.setString(10, user.getProfilePicture());
            statement.setInt(11, user.getId());
            statement.execute();
            updateShopIds(user);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void updateShopIds(User user) {
        deleteFromRouterByUserId(user.getId());
        insertInRouter(user);
    }

    public void updateRouter(Integer userId, int shopId) {
        String sql = "update user_shop set shop_id = ? where user_id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            if (userId == null) {
                statement.setNull(1, Types.INTEGER);
            } else {
                statement.setInt(1, shopId);
            }
            statement.setInt(2, userId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Creates a unique id for user_shop.
     *
     * @return id
     */
    private int setIdForUserShop() {
        if (findAllIdsForUserShop().isEmpty()) return 1;
        else return (findAllIdsForUserShop().get(findAllIdsForUserShop().size() - 1) + 1);
    }

    /**
     * Creates a unique id.
     *
     * @return id
     */
    private int setId() {
        if (findAll().isEmpty()) return 1;
        else return findAll().get(findAll().size() - 1).getId() + 1;
    }

}
