package com.example.demo.repository;

import com.example.demo.model.Expenses;
import com.example.demo.model.Product;
import com.example.demo.model.TypeOfExpense;
import com.example.demo.repository.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExpensesRepository {

    /**
     * Create a list with every expenses in the app.
     *
     * @return
     */
    public List<Expenses> findAll() {
        List<Expenses> expenses = new ArrayList<>();
        String sql = "select * from expenses";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String typeOfExpenses = resultSet.getString("type");
                String nameOfExpenses = resultSet.getString("name_of_expenses");
                int quantity = resultSet.getInt("quantity");
                double valuePerOne = resultSet.getDouble("value_per_one");
                double totalValue = resultSet.getDouble("total_value");
                expenses.add(new Expenses(id, TypeOfExpense.valueOf(typeOfExpenses), nameOfExpenses, quantity, valuePerOne, totalValue));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return expenses;
    }

    /**
     * Find a expense from the id you give.
     *
     * @param id is a expense id.
     * @return
     */
    public Expenses findById(int id) {
        String sql = "select * from expenses where id = " + id;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
            int returnedId = resultSet.getInt("id");
            String typeOfExpenses = resultSet.getString("type");
            String nameOfExpenses = resultSet.getString("name_of_expenses");
            int quantity = resultSet.getInt("quantity");
            double valuePerOne = resultSet.getDouble("value_per_one");
            double totalValue = resultSet.getDouble("total_value");
            return new Expenses(id, TypeOfExpense.valueOf(typeOfExpenses), nameOfExpenses, quantity, valuePerOne, totalValue);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    /**
     * insert a expense in expenses.
     *
     * @param expense is the expense you want to insert
     * @param shopId
     */
    public void insert(Expenses expense, int shopId) {
        expense.setId(setId());
        String sql = "insert into expenses (id, type, name_of_expenses, quantity, value_per_one, total_value, shop_id) values (?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, expense.getId());
            statement.setString(2, expense.getTypeOfExpenses().name());
            statement.setString(3, expense.getNameOfExpenses());
            statement.setInt(4, expense.getQuantity());
            statement.setDouble(5, expense.getValuePerOne());
            statement.setDouble(6, expense.getTotalValue());
            statement.setInt(7, shopId);
            statement.execute();
            insertExpenseInProduct(expense, shopId);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private void insertExpenseInProduct (Expenses expense, int shopId) {
        if (expense.getTypeOfExpenses().name().equals(TypeOfExpense.Product.name())) {
            String sql = "insert into product (id, name, price, quantity, shop_id) values (?, ?, ?, ?, ?)";
            try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, setIdForProduct());
                statement.setString(2, expense.getNameOfExpenses());
                statement.setDouble(3, expense.getValuePerOne());
                statement.setInt(4, expense.getQuantity());
                statement.setInt(5, shopId);
                statement.execute();
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
            }
        }
    }

    private int setIdForProduct() {
        if (findAllProducts().isEmpty()) return 1;
        else return findAllProducts().get(findAllProducts().size() - 1).getId() + 1;
    }

    public List<Product> findAllProducts() {
        List<Product> products = new ArrayList<>();
        String sql = "select * from product";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                double price = resultSet.getDouble("price");
                int quantity = resultSet.getInt("quantity");
                products.add(new Product(id, name, price, quantity));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return products;
    }


    /**
     * update a expense
     *
     * @param
     */
    public void update(Expenses expenses) {
        String sql = "update expenses set id = ? type = ? name_of_expenses = ? quantity = ? value_per_one = ? total_value = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, expenses.getId());
            statement.setString(2, expenses.getTypeOfExpenses().name());
            statement.setString(4, expenses.getNameOfExpenses());
            statement.setInt(4, expenses.getQuantity());
            statement.setDouble(5, expenses.getValuePerOne());
            statement.setDouble(6, expenses.getTotalValue());
            statement.setInt(7, expenses.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Delete a expense from the id you give.
     *
     * @param id
     */
    public void deleteById(int id) {
        String sql = "delete from expenses where id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteExpenseByShopId(int id) {
        String sql = "delete from expenses where shop_id = ?;";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Finds you the expenses list from the shop id you give.
     *
     * @param shopId
     * @return
     */
    public List<Expenses> findExpensesByShopId(int shopId) {
        List<Expenses> expenses = new ArrayList<>();
        String sql = "select * from expenses where shop_id = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String typeOfExpense = resultSet.getString("type");
                String nameOfExpenses = resultSet.getString("name_of_expenses");
                int quantity = resultSet.getInt("quantity");
                double valuePerOne = resultSet.getDouble("value_per_one");
                double totalValue = resultSet.getDouble("total_value");
                expenses.add(new Expenses(id, TypeOfExpense.valueOf(typeOfExpense), nameOfExpenses, quantity, valuePerOne, totalValue));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return expenses;
    }

    /**
     * Creates a unique id.
     *
     * @return id
     */
    private int setId() {
        if (findAll().isEmpty()) return 1;
        else return findAll().get(findAll().size() - 1).getId() + 1;
    }

}
