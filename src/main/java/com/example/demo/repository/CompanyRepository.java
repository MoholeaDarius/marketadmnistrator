package com.example.demo.repository;

import com.example.demo.model.Company;
import com.example.demo.repository.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyRepository {

    private ShopRepository shopRepository;

    public CompanyRepository(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    public Company findCompany(){
        String sql ="select * from company";
        try(Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                return new Company(id, name,shopRepository.findAll());
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }


}
